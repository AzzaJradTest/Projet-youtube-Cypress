export class LoginPage {

    email_txtbox='#Email'
    psswrd_txtbox='#Password'
    remember_checkbox='#RememberMe'
    login_btn='.button-1'
    msg_erreur='.message-error'

    saisirEmail(str) {
        cy.get(this.email_txtbox).clear()
        cy.get(this.email_txtbox).type(str)
    }
    saisirPassword(str) {
        cy.get(this.psswrd_txtbox).clear()
        cy.get(this.psswrd_txtbox).type(str)
    }
    cliquerRemember() {
        cy.get(this.remember_checkbox).click()
    }
    cliquerLogin() {
        cy.get(this.login_btn).click()
    }
    getMsgErreur() {
        return cy.get(this.msg_erreur)}
}