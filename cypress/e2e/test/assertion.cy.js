/// ＜reference types="cypress" /＞

it('tester une assertion', () => {

  cy.visit('https://example.cypress.io/')
  cy.contains('get').click()

  cy.get('#query-btn')
    .should('contain', 'Button')
    .should('have.class', 'query-btn btn btn-primary')
    .and('be.visible')

  expect(cy.get('#query-btn')).to.exist
  // expect(cy.get('#query-btn')).to.be.an('button')

  expect(true).to.be.true
  assert.equal(4, '4', 'vrai')
  //assert.strictequal(4, '4', 'vrai')
})
