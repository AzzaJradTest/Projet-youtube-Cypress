/// ＜reference types="cypress" /＞

it('tester nop commerce', () => {

    cy.visit('https://admin-demo.nopcommerce.com/')

    cy.get('#Email').clear()
    cy.get('#Password').clear()

    cy.wait(1000)

    cy.get('#Email').type("admin@yourstore.com")
    cy.get('#Password').type("admin")

    // cy.get('#APjFqb').type("polene  {Enter}")
    //cy.contains('The Must-Haves', {timeout:6000}).click

    cy.get('.button-1').click()

    // Should
    cy.get('.navbar-nav > :nth-child(2)').should('have.text', '\nJohn Smith\n');
    cy.get('.navbar-nav > :nth-child(2)').invoke('text').should('eq', '\nJohn Smith\n');
    // Expect
    cy.get('.navbar-nav > :nth-child(2)')
    .invoke('text')
    .then((txt) => {
        expect(txt.trim()).to.equal('John Smith');  // trim() :Supprime les espaces inutiles et les sauts de ligne 
    });

})