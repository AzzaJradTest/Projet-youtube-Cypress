import { beforeEach } from "mocha"
import { LoginPage} from "../Pages NopCommerce/LoginP"
import { DashBoardPage } from "../Pages NopCommerce/DashBoardP"

const loginPage = new LoginPage()
const dashBoardPage= new DashBoardPage()

describe ('vérifier page login NopCommerce ', ()=> {

    beforeEach(function() {
        cy.visit('https://admin-demo.nopcommerce.com/')
    })
    it('données valides', ()=>{
        
        cy.log('données valides')
        loginPage.saisirEmail('admin@yourstore.com')
        loginPage.saisirPassword('admin')
        loginPage.cliquerRemember()
        loginPage.cliquerLogin()
        
        let nom= dashBoardPage.getNom()
        nom.should('exist');
    })
    it('données non valides', ()=>{
        
        loginPage.saisirEmail('admin@yourstore')
        loginPage.saisirPassword('admin1111')
        loginPage.cliquerRemember()
        loginPage.cliquerLogin()

        let msg= loginPage.getMsgErreur()
        msg.should('contain', 'Login was unsuccessful');

       // msg.should('have.text', 'Login was unsuccessful. Please correct the errors and try again.No customer account found\n')
        
    })
})
